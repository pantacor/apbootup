#!/bin/sh

set -ex

IMGNAME=tmp
ARCH=armhf

dir=`dirname $0`

outfile=$1
outsq=${outfile:-out/export.squashfs}
rm -f $outsq

docker build -t $IMGNAME -f Dockerfile.$ARCH ${dir:-.}

did=`docker run -d $IMGNAME /bin/sleep 10000`
if test -z "$did"; then
   echo "error starting docker ..."
   exit 1
fi

outd="$dir/out"
mkdir -p $outd || true

docker export -o $outd/export.tar.gz "$did"

tmpd=`mktemp -d`
tar -C $tmpd -xf $outd/export.tar.gz
mksquashfs $tmpd/ $outsq -comp xz

docker stop $did
docker rm $did

rm -rf $tmpd

